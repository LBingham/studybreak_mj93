extends Node

const THOUGHT_PHRASES = {
	"studious":[
		"1786-1799",
		"Foundations of Democracy",
		"Abolition of Feudalism",
		"Liberte, egalite, fraternite",
		"Formatiuon of the Estates General",
		"Formation of the National Assembly",
		"Importance of La Maseillaise",
		"Execution of Louis XVI",
		"Invention of the Guillotine",
		"Abolition of the Ancien Regime"
	],
	"distracting":[
		"Oh Canada",
		"Hon Hon Baguette",
		"Mmmm, Pommes Frites",
		"Oui oui",
		"Miss the beach",
		"Not much of a spring Break ...",
		"Stupid Dr Venk",
		"Block Bash 2 is out",
		"I want cookies",
		"What if I were a worm",
		"Frogs ... Grenouille",
		"Oui oui, grenouille"
	],
	"jesse":[
		"Their eyes are so cool",
		"Their favorite drink is Dr Pepper",
		"Jesse's hoodie",
		"What will I wear to the date?",
		"Are they allergic to anything?",
		"What if we kiss?!",
		"I wanna hold your hand",
		"My heart is beating like crazy",
		"They wanted that spider necklace"
	],
	"jesse_call":["HOLY CRAP JESSE IS CALLING ME"]
}

enum ThoughtType {STUDIOUS, DISTRACTING, JESSE_CALL, JESSE}

onready var root = get_tree().get_root().get_node("Node2D")
var debugger
var LETTERS = []
var buttonMode = "hold"

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	var lettersString = "QWERTYUIOPASDFGHJKLZXCVBNM"
	for letter in lettersString:
		LETTERS.append(letter)
	
	if root != null:
		game_setup()
		debugger = root.get_node("Debugger")

func game_setup():
	pass
	# TODO: make this set the correct objects visible and such

func debug_label_text(labelName, thisText):
#	print("debugger called, text is: " + thisText)
#	print("labelName is: " + labelName)
	debugger.get_node(labelName).text = thisText
