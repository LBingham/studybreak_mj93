Stages of game
	Act 1: 3 minutes
	Phone call with Jessie, who is dating the player character
		Shot dialog segment
	Act 2: 3 minutes

Character is writing report on the French Revolution during spring break


Opener:
	It's not easy, staying focused on a task that one doesn't want to do. It's even harder when more appealing things are just outside the window. As you hear the waves of the beach lap against the rocks outside, your eyes turn ruefully to the screen. 
	"The Lasting Influence of the French Revolution"
