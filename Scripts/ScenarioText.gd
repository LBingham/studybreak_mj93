extends "Dialogues.gd"

onready var events = Global.root.get_node("Events")

var targetText
var currentString = ""
var stringIndex = 0
var paused = false
# 0: intro text. 1: tutorial text. 2: Jesse phone call 0,
# 3: Jesse phone call 1, 4: Jesse phone call 2, 5: Jesse phone call 3,
# 5: Jesse phone call 4, 6: Ending
var textStep = 0
var readyForNextScenarioStep = false

# Called when the node enters the scene tree for the first time.
func _ready():
	#set_text(INTRO_TEXT)
	
	Global.root.get_node("Title").connect("title_finished", self, "_on_title_finished")

func run_scenario_text(text):
	readyForNextScenarioStep = false
	$ContinueButton.visible = false
	$InputReminder.visible = true
	targetText = text
	currentString = ""
	stringIndex = 0
	set_text(currentString)
	unpause_printing()

func deactivate():
	$ContinueButton.visible = false
	$InputReminder.visible = false
	set_text("")

func set_text(text):
	$Panel/Label.bbcode_text = text

func process_next_letter():
	if stringIndex < targetText.length() - 1:
		var nextString = targetText[stringIndex]
		if nextString == "\n":
			pause_printing()
		stringIndex += 1
		currentString += nextString
		set_text(currentString)
	else:
		prepare_for_next_step()

func pause_printing():
	$NextLetter.stop()
	paused = true

func unpause_printing():
	$NextLetter.start()
	paused = false

func text_step_finished():
	# End of intro text
	if textStep == 0:
		textStep += 1
		run_scenario_text(TUTORIAL_TEXT)
	# End of tutorial text
	elif textStep == 1:
		textStep += 1
		events.begin_scenario(events.EVENTS.PRE_JESSE)
	# End of Jesse call
	elif textStep == 2:
		textStep += 1
		run_scenario_text(CALL_TEXT[1])
	elif textStep == 3:
		textStep += 1
		run_scenario_text(CALL_TEXT[2])
	elif textStep == 4:
		textStep += 1
		run_scenario_text(CALL_TEXT[3])
	elif textStep == 5:
		textStep += 1
		run_scenario_text(CALL_TEXT[4])
	elif textStep == 6:
		textStep += 1
		events.begin_scenario(events.EVENTS.POST_JESSE)
	elif textStep == 7:
		textStep += 1
		run_scenario_text(GAME_END)
	elif textStep == 8:
		activate_end_button()

func prepare_for_next_step():
	pause_printing()
	readyForNextScenarioStep = true
	$InputReminder.visible = false
	$ButtonWait.start()

func fade_out():
	$AnimationPlayer.play("Fade Out")

func fade_in():
	$AnimationPlayer.play("Fade In")

func activate_end_button():
	print("End button activated")
	$ContinueButton.visible = false
	#$GameEndButton.visible = true

func _input(event):
	if event is InputEventMouseButton && event.pressed && !readyForNextScenarioStep:
		if paused:
			if event.button_index == 1 && paused:
				unpause_printing()
		elif !$NextLetter.is_stopped():
			while !paused:
				_on_NextLetter_timeout()

func _on_NextLetter_timeout():
	process_next_letter()

func _on_title_finished(type):
	if type == "Out" && textStep == 0:
		run_scenario_text(INTRO_TEXT)

func _on_fade_in():
	if textStep == 2:
		run_scenario_text(CALL_TEXT[0])
		# TODO: Implement call text!
	elif textStep == 7:
		run_scenario_text(ENDING_TEXT)

func _on_ContinueButton_pressed():
	if modulate == Color.white:
		if readyForNextScenarioStep:
			text_step_finished()

func _on_ButtonWait_timeout():
#	print("timed out")
	if textStep != 8:
		$ContinueButton.visible = true
	else:
		activate_end_button()


func _on_GameEndButton_button_down():
	get_tree().quit()
