extends Node2D

const POINT_VARIATION_Y = 3

onready var currentLine = $Lines/Line1
onready var timer = $ActTimer

var dotIndex = 0
var progressScore = 100

signal page_finished

func _ready():
	for node in $Lines.get_children():
		node.points = PoolVector2Array([Vector2(0, 0)])

func progress_roll():
	var randRoll = rand_range(0, 100)
	if progressScore > randRoll:
		progress_line()
	else:
		regress_line()

func progress_line():
	var pointsArray = currentLine.points
	var currentVec = currentLine.points[dotIndex]
	currentVec.x += 5
	currentVec.y = rand_range(-1 * POINT_VARIATION_Y, POINT_VARIATION_Y)
	pointsArray.append(currentVec)
	currentLine.points = pointsArray
	
	dotIndex += 1
#	print("currentLine.points: " + String(currentLine.points))
	#currentLine.points[dotIndex] += Vector2(5, 0)
	if currentVec.x >= 90:
		var nextIndex = currentLine.get_index() + 1
		dotIndex = 0
		if nextIndex < $Lines.get_child_count():
			currentLine = $Lines.get_child(nextIndex)
		else:
			currentLine = $Lines/Line1
			$ActTimer.stop()
			$AnimationPlayer.play("Next Page")

func regress_line():
	if dotIndex != 0:
		var pointsArray = currentLine.points
		pointsArray.remove(dotIndex)
		currentLine.points = pointsArray
		dotIndex -= 1

func reset_lines():
	for node in $Lines.get_children():
		node.points = PoolVector2Array([Vector2(0, 0)])

func _on_ActTimer_timeout():
	progress_roll()

func set_progress_score(score):
	progressScore = score


func _on_AnimationPlayer_animation_finished(anim_name):
	emit_signal("page_finished")
	$ActTimer.start()
