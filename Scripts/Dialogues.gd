extends Node2D

const INTRO_TEXT = "The Student wished they weren\'t stuck inside, working.\n" + \
"They wished they could be out in the waves.\n" + \
"They wanted to be out in the sun.\n" + \
"Nope.\n" + \
"Another day of grinding away at that stupid history paper.\n" + \
"Another day of spring \"break\".\n" + \
"Ah well... at least the waves made for good background noise.\n"

const TUTORIAL_TEXT = "Thoughts come and go naturally.\n" + \
"Press the keys shown on the arrows as they appear.\n" + \
"This will grow studious thoughts, and shrink distracting thoughts.\n" + \
"Keep studious thoughts big and distracting thoughts small to write the paper."

const CALL_TEXT = [
	"Their hand shot for their phone as soon as they saw who it was.\n" + \
	"They had to clear their throat before they spoke. \"Hello?\"\n" + \
	"The voice coming from the other end was light and refreshing.\n" + \
	"It was one of the many things the Student liked about Jesse.\n" + \
	"\"Hey there! I'm glad you picked up! I figured I might miss ya.\n" + \
	"Thought you might be collecting shells all day.\"\n" + \
	"The Student gave a wry smirk to their self as they looked out the window.\n",
	
	"\"Yeah... no, I've been stuck inside all day. That paper for Venk.\"\n" + \
	"\"Ahhhhh damn, fair enough. I've been putting mine off, because, screw it!\"\n" + \
	"That got a chuckle out of the Student, and Jesse continued.\n" + \
	"\"Soooo anyways, we're grabbing coffee when you get back into town, okay?\"\n" + \
	"The Student balked, not quite sure they caught what Jesse said.\n" + \
	"\"Erm, I'm sorry, can you uh... can you say that again?\"\n" + \
	"A brief chuckle came from the receiver before, \"You. Me. Coffee date.\"\n",
	
	"\"I figured you would be the one asking me out after you got my number,\n" + \
	"but I've been wanting to hang out with you too much to keep waiting.\n" + \
	"There's that local place, what was it... Oh yeah, 'Killer Beans'. I've heard\n" + \
	"some good stuff about their coffee, so let's check them out when you're back\n" + \
	"in town, okay?\"\n" + \
	"The Student was still trying to catch up with the use of the word \"date\".\n" + \
	"Eventually, after a few moments of reeling, they came back to their senses.",
	
	"\"Y-yeah! Yeah that sounds... that sounds awesome, actually. I uh... how about\n" + \
	"Tuesday? 3pm after class? Does that work for you?\"\n" + \
	"A fair helping of excitement could be heard in Jesse's voice.\n" + \
	"\"Sounds perfect! Well, I don't want to keep you from straight A's, so I'll\n" + \
	"let ya get back to work on your paper. Get some sunlight today, okay?\n" + \
	"\"Yeah, yeah I ... really should do that, unfortunately. Sunlight and paper, both.\"\n" + \
	"Another light and lovely chuckle came through the receiver.\n" + \
	"\"Alright you, I'll talk to you another time. See ya!~\"\n", 
	
	"\"A-alright, have a good one!\" Beep.\n" + \
	"The phone didn't leave the side of the Student's head for several seconds.\n" + \
	"To him, the call was wonderfully unexpected."
]

const ENDING_TEXT = "\"Get some sunlight today, okay?\"\n" + \
	"That sentence didn't seem keen to leave.\n" + \
	"It stuck around in the Student's mind like it wanted to move in.\n" + \
	"For that matter, the prospect of a date with Jesse had them reeling,\n" + \
	"practically stuck in a sweet mire of thoughts.\n" + \
	"So, they pushed their chair back, sat up, and smiled.\n" + \
	"That was enough draft work for now. The other pages could wait."

const GAME_END = "Thank you so much for playing!\n" + \
	"I hope you enjoyed my entry into Mini Jam 93"
