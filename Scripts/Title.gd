extends Node2D

onready var root = get_tree().get_root().get_node("Node2D")

signal title_finished


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _on_title_faded():
	emit_signal("title_finished")

func _on_Button_pressed():
	$AnimationPlayer.play("Fade Out")
	$Button.disabled = true

func _on_AnimationPlayer_animation_finished(anim_name):
	var typeString = anim_name
	typeString.erase(0, 5)
	emit_signal("title_finished", typeString)

func _on_ModeButton_mouse_entered():
	$ModeButton/ModeDescription.visible = true

func _on_ModeButton_mouse_exited():
	$ModeButton/ModeDescription.visible = false


func _on_ModeButton_pressed():
	if Global.buttonMode == "hold":
		Global.buttonMode = "press"
		$ModeButton.text = "Mode: Press"
	elif Global.buttonMode == "press":
		Global.buttonMode = "hold"
		$ModeButton.text = "Mode: Hold"
