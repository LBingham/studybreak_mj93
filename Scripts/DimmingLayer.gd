extends Sprite

onready var titleNode = Global.root.get_node("Title")

var baseColor

signal fade_finished

# Called when the node enters the scene tree for the first time.
func _ready():
	var anim = $AnimationPlayer
	baseColor = modulate
	anim.get_animation("Fade Out").track_set_key_value(0, 0, baseColor)
	anim.get_animation("Fade In").track_set_key_value(0, 1, baseColor)

func fade_in():
	$AnimationPlayer.play("Fade In")

func fade_out():
	$AnimationPlayer.play("Fade Out")

func _on_AnimationPlayer_animation_finished(anim_name):
	var typeString 
