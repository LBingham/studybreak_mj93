extends Area2D

#TODO: add code to generate thought text from some file
const SPEED_MIN = 30#.05
const SPEED_MAX = 45#.1
const BASE_THOUGHT_WAIT_TIME = 2
const DISTRACTING_LIFETIME_MIN = 5
const DISTRACTING_LIFETIME_MAX = 10
const STUDIOUS_LIFETIME_MIN = 3
const STUDIOUS_LIFETIME_MAX = 8
const JESSE_LIFETIME_MIN = 10
const JESSE_LIFETIME_MAX = 15
const JESSE_THOUGHT_CHANCE = 50
const COLOR_LIST = [Color.blueviolet, Color.cadetblue, Color.darkcyan,
	Color.darkmagenta, Color.darkorchid]
const DISTRACTING = Global.ThoughtType.DISTRACTING
const STUDIOUS = Global.ThoughtType.STUDIOUS
const JESSE = Global.ThoughtType.JESSE
const JESSE_CALL = Global.ThoughtType.JESSE_CALL

var pLoadStudiousSprite = preload("res://Sprites/StudiousThought.png")
var pLoadDistractingSprite = preload("res://Sprites/DistractionThought.png")
var pLoadJesseSprite = preload("res://Sprites/JesseThought.png")

onready var paperMeter = get_parent().get_node("PaperMeter")
onready var titleNode = get_parent().get_node("Title")
onready var sounds = get_parent().get_node("Sounds")

var active = false
var thoughtMax = {Global.ThoughtType.STUDIOUS: 1, 
Global.ThoughtType.DISTRACTING: 2}
var thoughtCount = {Global.ThoughtType.STUDIOUS: 0, 
Global.ThoughtType.DISTRACTING: 0, Global.ThoughtType.JESSE: 0,
Global.ThoughtType.JESSE_CALL: 0}
# Key is object, value is button to be pressed for objects
var lettersToThoughts = {}
var nextThought
var makeJesseThoughts = false
var progressScore
var pLoadThought = preload("res://Packed/Thought.tscn")
# Key value is object, value is a a letter
#var thoughtButtonMap = {}
var pressedButtons = []

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _physics_process(delta):
	for letter in pressedButtons:
		if lettersToThoughts.keys().has(letter):
			for thought in lettersToThoughts[letter]:
				if thought != null:
					if Global.buttonMode == "press":
						thought.accessibilityKeyPressed = true
					else:
						thought.input_effect(delta)
						
					thought.get_node("AnimationPlayer").play("Text Flash")
					
	# Assess progress score
	progressScore = 0
	for node in $Thoughts.get_children():
		if node.thoughtType == Global.ThoughtType.STUDIOUS:
			progressScore += node.scale.x * 2
		else:
			progressScore -= node.scale.x
	paperMeter.progressScore = progressScore * 100 + 25
	#Global.debug_label_text("Label", "tBMap: " + String(thoughtButtonMap))
	Global.debug_label_text("Label", "LTThoughts: " + String(lettersToThoughts))

func assess_status():
#	print("assessing")
	var assessmentDict = thoughtCount.duplicate()
	var disCount = assessmentDict[DISTRACTING]
	var studCount = assessmentDict[STUDIOUS]
	if assessmentDict[STUDIOUS] >= 1 && \
	assessmentDict[JESSE] + assessmentDict[DISTRACTING] >= thoughtMax[DISTRACTING]:
		$TimerThoughtWait.start()
	elif studCount == 0:#hackedStudiousCount == 0:
		# Create studious thought to start with
		make_thought(Global.ThoughtType.STUDIOUS)
	elif disCount <= studCount:
		# Ensure that distracting thoughts outnumber studious thoughts
		make_thought(Global.ThoughtType.DISTRACTING)
	elif studCount < thoughtMax[STUDIOUS]:
		# If we don't have the max number of studious thoughts
		make_thought(Global.ThoughtType.STUDIOUS)

func make_thought(thoughtType):
#	print("calling make with type: " + String(thoughtType))
	# Create velocity
	var randAng = rand_range(0, 2 * PI)
	var speedVec = Vector2(rand_range(SPEED_MIN, SPEED_MAX), 0)
	nextThought = pLoadThought.instance()
	# If it's a distracting thought, check if we can potentially make it into
	# a Jesse thought
	if makeJesseThoughts && thoughtType == DISTRACTING:
		var randRoll = rand_range(0, 100)
		if randRoll > JESSE_THOUGHT_CHANCE:
			thoughtType = JESSE
	# Decide lifespan, set indicator visibility, set sprite texture
	var thisTexture
	var randTime
	if thoughtType == DISTRACTING:
		randTime = rand_range(DISTRACTING_LIFETIME_MIN, DISTRACTING_LIFETIME_MAX)
		nextThought.get_node("GrowIndicator").visible = false
		thisTexture = pLoadDistractingSprite
	elif thoughtType == STUDIOUS:
		randTime = rand_range(STUDIOUS_LIFETIME_MIN, STUDIOUS_LIFETIME_MAX)
		nextThought.get_node("ShrinkIndicator").visible = false
		thisTexture = pLoadStudiousSprite
	elif thoughtType == JESSE:
		randTime= rand_range(JESSE_LIFETIME_MIN, JESSE_LIFETIME_MAX)
		nextThought.get_node("GrowIndicator").visible = false
		thisTexture = pLoadJesseSprite
	elif thoughtType == JESSE_CALL:
		randTime= rand_range(JESSE_LIFETIME_MIN, JESSE_LIFETIME_MAX)
		nextThought.get_node("GrowIndicator").visible = false
		nextThought.get_node("ShrinkIndicator").visible = false
		thisTexture = pLoadJesseSprite
	# Decide input key for thought
	var randIndex = round(rand_range(0, Global.LETTERS.size() - 1))
	var inputLetter = Global.LETTERS[randIndex]
	nextThought.initialize(thoughtType, speedVec, randTime, inputLetter, thisTexture)
	$TimerThoughtCreate.start()

func make_jesse_call_thought():
	make_thought(JESSE_CALL)
	$TimerThoughtWait.wait_time = BASE_THOUGHT_WAIT_TIME
	$TimerThoughtWait.start()

func remove_thought(inst, thoughtType):
	var thisLetter = inst.inputLetter
	lettersToThoughts[thisLetter].erase(inst)
	if lettersToThoughts[thisLetter] == []:
		lettersToThoughts.erase(thisLetter)
	thoughtCount[thoughtType] = thoughtCount[thoughtType] - 1
	#If it's a productive thought leaving, bring a new productive thought in quicker
	$TimerThoughtWait.wait_time = .5
	$TimerThoughtWait.start()
#	Global.set_debug_label_text("Label", String(lettersToThoughts))
#	Global.debugger.get_node("Label").text = String(thoughtsToButtons)

func add_thought(inst, thoughtType):
#	print("Adding thought of type: " + String(thoughtType))
	var thisLetter = inst.inputLetter
	if !lettersToThoughts.has(thisLetter):
		lettersToThoughts[thisLetter] = [inst]
	else:
		lettersToThoughts[thisLetter].append(inst)
	thoughtCount[thoughtType] = thoughtCount[thoughtType] + 1
	# Stop making other thoughts if it's the jesse call thought
#	Global.set_debug_label_text("Label", String(lettersToThoughts))

func deploy_thought():
#	print("calling deploy_thought")
	add_thought(nextThought, nextThought.thoughtType)
	$Thoughts.add_child(nextThought)
	$TimerThoughtWait.wait_time = BASE_THOUGHT_WAIT_TIME
	$TimerThoughtWait.start()

func _input(event):
	if event is InputEventKey && !event.echo:
#		print(OS.get_scancode_string(event.scancode))
		var thisScanCodeString = OS.get_scancode_string(event.scancode)
		if event.pressed:
			if !pressedButtons.has(thisScanCodeString):
				pressedButtons.append(thisScanCodeString)
		else:
			pressedButtons.erase(thisScanCodeString)

		#Global.debugger.get_node("Label").text = String(pressedButtons)

func set_active(boolean):
	if boolean:
		thoughtCount = {Global.ThoughtType.STUDIOUS: 0, 
		Global.ThoughtType.DISTRACTING: 0, Global.ThoughtType.JESSE: 0,
		Global.ThoughtType.JESSE_CALL: 0}
		print("after activation, count is: " + String(thoughtCount))
		assess_status()
		set_physics_process(true)
		paperMeter.timer.start()
		sounds.start_typing()
	else:
		print("Calling stop")
		$TimerThoughtCreate.stop()
		$TimerThoughtWait.stop()
		paperMeter.timer.stop()
		sounds.stop_typing()
		set_physics_process(false)
		# Remove all thoughts and set traking dicts back to zero
		var lettersToThoughts = {}
		for child in $Thoughts.get_children():
			# This is hacky as shiiiit
			child.queue_free()
		thoughtCount = {Global.ThoughtType.STUDIOUS: 0, 
		Global.ThoughtType.DISTRACTING: 0, Global.ThoughtType.JESSE: 0,
		Global.ThoughtType.JESSE_CALL: 0}

func _on_TimerThoughtCreate_timeout():
#	print("Create timed out")
	deploy_thought()

func _on_TimerThoughtWait_timeout():
	print("Wait timed out")
	assess_status()

func begin_thinking():
	$AnimationPlayer.play("Fade In")

func fade_in():
	$AnimationPlayer.play("Fade In")

func fade_out():
	$AnimationPlayer.play("Fade Out")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Fade In":
		set_active(true)

