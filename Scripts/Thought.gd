extends Area2D

#TODO: add rate at which thought naturally shrinks or grows
const THOUGHT_BOUNCE_ANGLE = PI/4
const EDGE_BOUNCE_ANGLE = PI/4
const STUDIOUS_GROW_RATE = .15
const STUDIOUS_SHRINK_RATE = .1
const DISTRACTION_GROW_RATE = .15
const DISTRACTION_SHRINK_RATE = .3
const JESSE_GROW_RATE = .15
const JESSE_SHRINK_RATE = .2
const JESSE_CALL_GROW_RATE = .3
const STARTING_SCALE = Vector2(.5, .5)
const MIN_SCALE = Vector2(.25, .25)
const MAX_SCALE = Vector2(1, 1)
const MAX_JESSE_SCALE = Vector2(5, 5)

onready var myEnvironment = get_parent().get_parent()

export(Vector2) var baseScale = Vector2(2, 2)

var COLOR_LIST
var velocity = Vector2(0,0)
var thoughtType
var inputLetter
var accessibilityKeyPressed = false

# Called when the node enters the scene tree for the first time.
func _ready():
	COLOR_LIST = get_parent().get_parent().COLOR_LIST

func _physics_process(delta):
	position += velocity * delta
	if thoughtType == Global.ThoughtType.STUDIOUS:
		if scale.x > MIN_SCALE.x:
			set_scale(scale - make_vec(STUDIOUS_SHRINK_RATE) * delta)
	elif thoughtType == Global.ThoughtType.DISTRACTING:
		if scale.x < MAX_SCALE.x:
			set_scale(scale + make_vec(DISTRACTION_GROW_RATE) * delta)
	elif thoughtType == Global.ThoughtType.JESSE:
		if scale.x < MAX_SCALE.x:
			set_scale(scale + make_vec(JESSE_GROW_RATE) * delta)
	elif thoughtType == Global.ThoughtType.JESSE_CALL:
		if scale.x < MAX_JESSE_SCALE.x:
			set_scale(scale + make_vec(JESSE_CALL_GROW_RATE) * delta)
	
	# Act out accessibility option
	if accessibilityKeyPressed:
		input_effect(delta)

func initialize(thisType, thisVel, lifeTime, thisInput, texture):
	inputLetter = thisInput
	thoughtType = thisType
	$DeathTimer.wait_time = lifeTime
	velocity = thisVel
	$Sprite.texture = texture
	set_scale(STARTING_SCALE)
	# Thought type specific functions
	# Color, input letter, scale
	if thoughtType == Global.ThoughtType.STUDIOUS:
		var size = Global.THOUGHT_PHRASES.studious.size()
		var randIndex = round(rand_range(0, size - 1))
		var randPhrase = Global.THOUGHT_PHRASES.studious[randIndex]
		set_text(randPhrase)
		#$Sprite.modulate = Color.darkcyan
		$GrowIndicator/PanelContainer/Label.text = inputLetter
	elif thoughtType == Global.ThoughtType.DISTRACTING:
		var size = Global.THOUGHT_PHRASES.distracting.size()
		var randIndex = round(rand_range(0, size - 1))
		var randPhrase = Global.THOUGHT_PHRASES.distracting[randIndex]
		set_text(randPhrase)
		#$Sprite.modulate = Color.darkred
		$ShrinkIndicator/PanelContainer/Label.text = inputLetter
	elif thoughtType == Global.ThoughtType.JESSE:
		var size = Global.THOUGHT_PHRASES.jesse.size()
		var randIndex = round(rand_range(0, size - 1))
		var randPhrase = Global.THOUGHT_PHRASES.jesse[randIndex]
		set_text(randPhrase)
		$PanelContainer/RichTextLabel.modulate = Color.darkgray
		#$Sprite.modulate = Color.darkmagenta
		$ShrinkIndicator/PanelContainer/Label.text = inputLetter
	elif thoughtType == Global.ThoughtType.JESSE_CALL:
		velocity = Vector2(0,0)
		set_text(Global.THOUGHT_PHRASES.jesse_call[0])
	# TODO: get thought text dependent on the thought type we decide on

func input_effect(delta):
	if thoughtType == Global.ThoughtType.STUDIOUS:
		if scale.x < MAX_SCALE.x:
			set_scale(scale + make_vec(STUDIOUS_GROW_RATE) * delta)
	elif thoughtType == Global.ThoughtType.DISTRACTING:
		if scale.x > MIN_SCALE.x:
			set_scale(scale - make_vec(DISTRACTION_SHRINK_RATE) * delta)
	elif thoughtType == Global.ThoughtType.JESSE:
		if scale.x > MIN_SCALE.x:
			set_scale(scale - make_vec(JESSE_SHRINK_RATE) * delta)

func _on_Thought_area_entered(area):
	# If it hits the edge of the environment
	if area.name == "ThoughtEnvironment":
		var midPoint = get_parent().global_position
		var randAng = rand_range(-1 * EDGE_BOUNCE_ANGLE, EDGE_BOUNCE_ANGLE)
		var vecTowardsMid = (midPoint - global_position).normalized()
		velocity = vecTowardsMid.rotated(randAng) * velocity.length()
	# If it has clipped out of the normal thought environment
	elif area.name == "DestroyZone":
		if thoughtType == Global.ThoughtType.JESSE_CALL:
			print("Has Jesse!")
			var midPoint = get_parent().global_position
			var randAng = rand_range(-1 * EDGE_BOUNCE_ANGLE, EDGE_BOUNCE_ANGLE)
			var vecTowardsMid = (midPoint - global_position).normalized()
			velocity = vecTowardsMid.rotated(randAng) * velocity.length()
		else:
			destroy()
	# If it hits another thought
	else:
		var midPoint = area.global_position
		var randAng = rand_range(-1 * THOUGHT_BOUNCE_ANGLE, THOUGHT_BOUNCE_ANGLE)
		var vecAwayFromMid = (global_position - midPoint).normalized()
		
		velocity = vecAwayFromMid.rotated(randAng) * velocity.length()

func set_text(text):
	$PanelContainer/RichTextLabel.bbcode_text ="[center]" + text + "[center]"

func set_scale(newScale : Vector2):
	var scaleDiff = newScale - scale
	# TODO: GET RID OF THIS
	# Reparent nodes so you're not scaling the grow and shrink indicators
	scale = newScale

func destroy():
	myEnvironment.remove_thought(self, thoughtType)
	queue_free()

func make_vec(val):
	return Vector2(val, val)

func _on_DeathTimer_timeout():
	destroy()
