extends Node2D

var baseTypingVolume

# Called when the node enters the scene tree for the first time.
func _ready():
	baseTypingVolume = $TypingSounds.volume_db

func stop_typing():
	print("stop called")
	$TypingSounds.volume_db = -60

func start_typing():
	print("start called")
	$TypingSounds.play()
	$TypingSounds.volume_db = baseTypingVolume

func start_vibrating():
	$PhoneSounds.play()

func stop_vibrating():
	$VibrateTimer.stop()
	$PhoneSounds.stop()

func _on_OceanSounds_finished():
	$OceanSounds.play(0.0)

func _on_TypingSounds_finished():
	$TypingSounds.play(0.0)

func _on_PhoneSounds_finished():
	$VibrateTimer.start()

func _on_VibrateTimer_timeout():
	$PhoneSounds.play()

