extends Node

enum EVENTS {PRE_JESSE, PHONE_RING, JESSE_CALL, POST_JESSE, ENDING}
onready var root = Global.root
onready var scenarioText = root.get_node("ScenarioText")
onready var dimmingLayer = root.get_node("DimmingLayer")
onready var thoughtEnvironment = root.get_node("ThoughtEnvironment")
onready var sounds = root.get_node("Sounds")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func begin_scenario(event):
	if event == EVENTS.PRE_JESSE:
		$JesseTimer.start()
		scenarioText.fade_out()
		dimmingLayer.fade_out()
		thoughtEnvironment.begin_thinking()
	elif event == EVENTS.JESSE_CALL:
		print("Jesse is calling!")
		thoughtEnvironment.make_jesse_call_thought()
		#sounds.stop_typing()
		$JesseCallCountdown.start()
	elif event == EVENTS.POST_JESSE:
		print("Post Jesse time")
		scenarioText.fade_out()
		dimmingLayer.fade_out()
		thoughtEnvironment.begin_thinking()
		thoughtEnvironment.makeJesseThoughts = true
		$EndgameTimer.start()
	elif event == EVENTS.ENDING:
		print("WRAP IT UP, BABY!")
		sounds.stop_typing()
		thoughtEnvironment.set_active(false)
		thoughtEnvironment.fade_out()
		dimmingLayer.fade_in()
		scenarioText.fade_in()
	elif event == EVENTS.PHONE_RING:
		root.get_node("LitPhone").visible = true
		sounds.start_vibrating()

func _on_JesseTimer_timeout():
	print("Jesse calling!")
	$JesseRingResponseTimer.start()
	begin_scenario(EVENTS.PHONE_RING)

func _on_JesseCallCountdown_timeout():
	thoughtEnvironment.set_active(false)
	thoughtEnvironment.fade_out()
	sounds.stop_vibrating()
	root.get_node("LitPhone").visible = false
	dimmingLayer.fade_in()
	scenarioText.fade_in()

func _on_EndgameTimer_timeout():
	begin_scenario(EVENTS.ENDING)

func _on_JesseRingResponseTimer_timeout():
	begin_scenario(EVENTS.JESSE_CALL)
